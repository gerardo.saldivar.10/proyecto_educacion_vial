import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:tsp/backend/database.dart';

class WidgetGraficas extends StatefulWidget {
  WidgetGraficas({Key key}) : super(key: key);

  @override
  _WidgetGraficasState createState() => _WidgetGraficasState();
}

class _WidgetGraficasState extends State<WidgetGraficas> {
  var texto = "Presiona el botón flotante para mostrar la gráfica";
  bool toggle = false;
  final Map<String, String> mesesAnio = {
    "1": "Enero",
    "2": "Febrero",
    "3": "Marzo",
    "4": "Abril",
    "5": "Mayo",
    "6": "Junio",
    "7": "Julio",
    "8": "Agosto",
    "9": "Septiembre",
    "10": "Octubre",
    "11": "Noviembre",
    "12": "Diciembre"
  };

  Map<String, double> dataMapNot = Map();

  List<Color> colorList = [
    Colors.red,
    Colors.green,
    Colors.blue,
    Colors.yellow,
    Colors.greenAccent,
    Colors.indigo,
    Colors.orange,
    Colors.pink,
    Colors.purple,
    Colors.redAccent,
    Colors.yellowAccent,
    Colors.blueAccent,
    Colors.black26,
    Colors.blueGrey,
    Colors.deepPurpleAccent,
    Colors.lime,
  ];

  _databaseOpening() async {
    DB db = new DB();
    var temp = await db.obtenerRutaDBEstadisticas();
    await db.consulta(temp, 'SELECT dia, mes, anio, COUNT(*) FROM fechas GROUP BY mes').then((value) {
      value.forEach((element) { 
        dataMapNot.putIfAbsent(mesesAnio[element['mes']] + " " + element['anio'], () => element['COUNT(*)']+.0);
      });
    });
    print(dataMapNot);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _databaseOpening();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Gráfica de manejo"),
      ),
      body: Container(
        child: Center(
          child: toggle && dataMapNot.isNotEmpty
              ? PieChart(
                  dataMap: dataMapNot,
                  animationDuration: Duration(milliseconds: 800),
                  chartLegendSpacing: 30.0,
                  chartRadius: MediaQuery.of(context).size.width / 2.0,
                  showChartValuesInPercentage: false,
                  showChartValues: true,
                  showChartValuesOutside: true,
                  chartValueBackgroundColor: Colors.grey[200],
                  colorList: colorList,
                  showLegends: true,
                  legendPosition: LegendPosition.bottom,
                  decimalPlaces: 0,
                  showChartValueLabel: true,
                  legendStyle: TextStyle(fontSize: 15.0),
                  initialAngle: 0,
                  chartValueStyle: defaultChartValueStyle.copyWith(
                    color: Colors.blueGrey[900].withOpacity(0.9),
                  ),
                  chartType: ChartType.ring,
                )
              : Text(texto),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: togglePieChart,
        child: Icon(Icons.insert_chart),
      ),
    );
  }

  void togglePieChart() {
    setState(() {
      toggle = !toggle;
      if(dataMapNot.isEmpty){
        texto = 'Es posible que no hayan datos aún';
      }
    });
  }
}
