import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tsp/backend/database.dart';

class EditarLicencia extends StatefulWidget {
  @override
  _EditarLicenciaState createState() => _EditarLicenciaState();
}

class _EditarLicenciaState extends State<EditarLicencia> {
  @override

  Map data = {};
  final dateFormatDB = new DateFormat('dd-MM-yyyy');
  String nombre;
  DateTime _nacimiento;
  DateTime _vencimiento;
  String nacimientoDB;
  String vencimientoDB;

  void initState(){
    super.initState();
  }

  _updateLicencia() async {
    DB db = new DB();
    var temp = await db.obtenerRutaDBEstadisticas();
    await db.insertLicencia(temp, nombre, nacimientoDB, vencimientoDB);
  }

  void updateLicencia() async {
    nacimientoDB = dateFormatDB.format(_nacimiento);
    vencimientoDB = dateFormatDB.format(_vencimiento);
    await _updateLicencia();
    Navigator.popAndPushNamed(context, '/perfil');
  }

  Widget build(BuildContext context) {
    data = data.isNotEmpty ? data : ModalRoute.of(context).settings.arguments;
    nombre = data['nombre'];
    print(nombre);
    nacimientoDB = data['nacimiento'];
    vencimientoDB = data['vencimiento'];
    if (nacimientoDB != null){
      _nacimiento = dateFormatDB.parse(nacimientoDB);
    }
    print(_nacimiento);
    if (vencimientoDB != null){
      _vencimiento = dateFormatDB.parse(vencimientoDB);
    }
    print(_vencimiento);
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Text('Licencia'),
        centerTitle: true,
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                width: double.infinity,
                child: TextFormField(
                  initialValue: nombre == null ? null : nombre,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Nombre: ",
                    labelStyle: TextStyle(
                      fontSize: 20,
                      color: Colors.grey[700],
                    ),
                    hintText: "Escribe un nombre, por favor.",
                  ),
                  onChanged: (text) {
                    setState(() {
                      data['nombre'] = text;
                    });
                  },
                ),
              ),
              SizedBox(height: 10),
              Text(
                "Fecha de Nacimiento:",
                style: TextStyle(
                  color: Colors.indigo[500],
                  fontSize: 24,
                ),
              ),
              SizedBox(height: 10),
              Container(
                child: Row(
                  children: <Widget>[
                    Text(
                      _nacimiento == null ? "${dateFormatDB.format(DateTime.now())}" : "${dateFormatDB.format(_nacimiento)}",
                      style: TextStyle(
                        color: Colors.grey[900],
                        fontSize: 20,
                      ),
                    ),
                    SizedBox(width: 10),
                    IconButton(
                      color: Colors.blue,
                      icon: Icon(
                          Icons.calendar_today
                      ),
                      onPressed: (){
                        showDatePicker(
                          context: context,
                          initialDate: _nacimiento == null ? DateTime.now() : _nacimiento,
                          firstDate: DateTime(1920),
                          lastDate: DateTime(2220)
                        ).then((ndate){
                          setState(() {
                            _nacimiento = ndate;
                            data['nacimiento'] = dateFormatDB.format(_nacimiento);
                          });
                        });
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10),
              Text(
                "Fecha de Vencimiento:",
                style: TextStyle(
                  color: Colors.indigo[500],
                  fontSize: 24,
                ),
              ),
              Container(
                child: Row(
                  children: <Widget>[
                    Text(
                      _vencimiento == null ? "${dateFormatDB.format(DateTime.now())}" : "${dateFormatDB.format(_vencimiento)}",
                      style: TextStyle(
                        color: Colors.grey[900],
                        fontSize: 20,
                      ),
                    ),
                    SizedBox(width: 10),
                    IconButton(
                      color: Colors.blue,
                      icon: Icon(
                          Icons.calendar_today
                      ),
                      onPressed: (){
                        showDatePicker(
                            context: context,
                            initialDate: _vencimiento == null ? DateTime.now() : _vencimiento,
                            firstDate: DateTime(1920),
                            lastDate: DateTime(2220)
                        ).then((vdate){
                          setState(() {
                            _vencimiento = vdate;
                            data['vencimiento'] = dateFormatDB.format(_vencimiento);
                          });
                        });
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(height: 20),
              Align(
                child: Center(
                  child: RaisedButton(
                    color: Colors.blue,
                    child: Text(
                      "Guardar",
                      style: TextStyle(
                        color: Colors.grey[100],
                      ),
                    ),
                    onPressed: (){
                      updateLicencia();
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
