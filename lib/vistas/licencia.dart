import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tsp/backend/database.dart';
import 'package:tsp/backend/notifications.dart';

class Licencia extends StatefulWidget {
  @override
  _LicenciaState createState() => _LicenciaState();
}

class _LicenciaState extends State<Licencia> {
  @override

  String nombre;
  String nacimientoDB;
  String vencimientoDB;
  DateTime vencimiento;
  Notifications notification = new Notifications();
  final dateFormatDB = new DateFormat('dd-MM-yyyy');

  Map<String, dynamic> data = {
    'nombre': null,
    'nacimiento': null,
    'vencimiento': null,
  };

  void initState(){
    _databaseOpening();
    super.initState();
  }

  void checkVencimiento(){
    print(vencimientoDB);
    if (vencimientoDB != null){
      vencimiento = dateFormatDB.parse(vencimientoDB);
      print("$vencimiento");
      if (vencimiento.isBefore(DateTime.now())){
        print("Si se pudo. Así es");
        notification.notificacionLicenciaVencida();
      }
    }
  }

  _databaseOpening() async {
    DB db = new DB();
    var temp = await db.obtenerRutaDBEstadisticas();
    try{
      await db.consulta(temp, "SELECT nombre, nacimiento, vencimiento FROM licencias WHERE id=1").then((value){
        print(value.toList()[0]);
        data = value.toList()[0];
        print(data);
      });
    }
    catch(RangeError){
      print("No hay datos aún");
    }
    nombre = data['nombre'];
    nacimientoDB = data['nacimiento'];
    vencimientoDB = data['vencimiento'];
    checkVencimiento();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Text('Licencia'),
        centerTitle: true,
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: FutureBuilder(
          future: Future.delayed(Duration(seconds: 3)),
          builder: (c, s) => s.connectionState != ConnectionState.done
              ? Center(child: Text('Cargando'))
              : Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Nombre:",
                style: TextStyle(
                  color: Colors.indigo[500],
                  fontSize: 24,
                ),
              ),
              SizedBox(height: 10),
              Container(
                child: Text(
                  "${data['nombre']}",
                  style: TextStyle(
                    color: Colors.grey[900],
                    fontSize: 20,
                  ),
                ),
              ),
              SizedBox(height: 10),
              Text(
                "Fecha de Nacimiento:",
                style: TextStyle(
                  color: Colors.indigo[500],
                  fontSize: 24,
                ),
              ),
              SizedBox(height: 10),
              Container(
                child: Text(
                  "${data['nacimiento']}",
                  style: TextStyle(
                    color: Colors.grey[900],
                    fontSize: 20,
                  ),
                ),
              ),
              SizedBox(height: 10),
              Text(
                "Fecha de Vencimiento:",
                style: TextStyle(
                  color: Colors.indigo[500],
                  fontSize: 24,
                ),
              ),
              SizedBox(height: 10),
              Container(
                child: Text(
                  "${data['vencimiento']}",
                  style: TextStyle(
                    color: Colors.grey[900],
                    fontSize: 20,
                  ),
                ),
              ),
              SizedBox(height: 10),
              Text(
                data['nombre'] != null && data['nacimiento'] != null && data['vencimiento'] != null ? "" : "No has registrado tu licencia. Presiona el botón editar",
                style: TextStyle(
                  color: Colors.red[900],
                ),
              ),
              SizedBox(height: 10),
              Align(
                child: Center(
                  child: RaisedButton(
                    color: Colors.blue,
                    child: Text(
                      "Editar",
                      style: TextStyle(
                        color: Colors.grey[100],
                      ),
                    ),
                    onPressed: (){
                      Navigator.popAndPushNamed(context, '/perfil/editar', arguments: {
                        'nombre': nombre,
                        'nacimiento': nacimientoDB,
                        'vencimiento': vencimientoDB,
                      });
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
