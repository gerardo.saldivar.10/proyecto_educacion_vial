import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tsp/backend/current_location_test.dart';
import 'package:provider/provider.dart';
import 'package:tsp/backend/noise_checker.dart';
import 'package:tsp/backend/check_gasoline_meter.dart';
import 'package:tsp/backend/notifications.dart';

class SpeedStreetVelocityView extends StatelessWidget {
  const SpeedStreetVelocityView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      MapWidget(),
      Container(
        height: 100.0,
        alignment: Alignment.topCenter,
        decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.grey),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SpeedLabelText(),
            Divider(
              color: Colors.black,
              thickness: 3.0,
              endIndent: 130.0,
              indent: 130.0,
            ),
            SpeedStreetLabelText(),
          ],
        ),
      ),
      Padding(
          padding: EdgeInsets.only(top: 105.0), child: CheckBoxVoiceNotif()),
      Padding(
        padding: EdgeInsets.only(top: 485.0, left: 75.0),
        child: IsBadDirectionLabelText(),
      ),
    ]);
    /*return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [

        /*CheckBoxVoiceNotif(),
        Padding(
          padding: EdgeInsets.fromLTRB(0.0, 25.0, 0.0, 20.0),
          child: Center(child: SpeedLabelText()),
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 20.0),
          child: Center(
            child: SpeedStreetLabelText(),
          ),
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 20.0),
          child: Center(
            child: IsBadDirectionLabelText(),
          ),
        ),
        Divider(
          height: 0.0,
          color: Colors.black,
          thickness: 2.0,
        ),*/
        /*Stack(
          
          children: [
            Center(child: SpeedStreetLabelText()),
          ],
        ),*/
        Expanded(
          child: MapWidget(),
        ),
      ],
    );*/
  }
}

class SpeedLabelText extends StatelessWidget {
  Color velocityColor;

  @override
  Widget build(BuildContext context) {
    final sensor = Provider.of<GpsSensor>(context);

    if (sensor.speed != null) {
      if (sensor.speed >= 20) {
        if ((int.parse(sensor.speedStreet) - sensor.speed <= 10 &&
            (int.parse(sensor.speedStreet) - sensor.speed) > -10)) {
          velocityColor = Colors.yellow;
        } else if ((int.parse(sensor.speedStreet) - sensor.speed <= -10)) {
          velocityColor = Colors.red;
        } else {
          velocityColor = Colors.black;
        }
      } else {
        velocityColor = Colors.black;
      }
    }

    return Text(
      "${sensor.speed} KMH",
      style: TextStyle(
        color: velocityColor,
        fontWeight: FontWeight.bold,
        fontSize: 15.0,
      ),
    );
  }
}

class SpeedStreetLabelText extends StatelessWidget {
  const SpeedStreetLabelText({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final sensor = Provider.of<GpsSensor>(context);

    return Text(
      /*"La velocidad actual en este tramo es: " +
          "${sensor.speedStreet}" +
          " KMH",*/
      "${sensor.speedStreet} KMH",
      style: TextStyle(
        color: Colors.black,
        fontWeight: FontWeight.bold,
        fontSize: 15.0,
      ),
    );
  }
}

class IsBadDirectionLabelText extends StatelessWidget {
  const IsBadDirectionLabelText({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final sensor = Provider.of<GpsSensor>(context);
    return sensor.isBadDirection != null && sensor.isBadDirection
        ? Text(
            "Cuidado, sentido incorrecto",
            style: TextStyle(
                color: Colors.red, fontSize: 17.0, fontWeight: FontWeight.bold),
          )
        : Container();
  }
}

class CheckBoxVoiceNotif extends StatefulWidget {
  CheckBoxVoiceNotif({Key key}) : super(key: key);

  @override
  _CheckBoxVoiceNotifState createState() => _CheckBoxVoiceNotifState();
}

class _CheckBoxVoiceNotifState extends State<CheckBoxVoiceNotif> {
  bool value = false;

  @override
  Widget build(BuildContext context) {
    final sensor = Provider.of<GpsSensor>(context);
    final microphone = Provider.of<NoiseMeterChecker>(context);
    final gasolina = Provider.of<CheckGasolineMeter>(context);

    if (sensor.isVoiceNotifEnabled == null) {
      sensor.voiceNotifCheckBox = value;
    }
    if (microphone.isVoiceNotifEnabled == null){
      microphone.voiceNotifCheckBox = value;
    }

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(
            "activar notificaciones de voz",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          Checkbox(
            value: value,
            onChanged: (bool _value) {
              sensor.voiceNotifCheckBox = _value;
              microphone.voiceNotifCheckBox = _value;
              gasolina.voiceNotifCheckBox= _value;
              setState(() {
                value = _value;
              });
            },
          ),
          //Center(child: Text(microphone.valueRecording.toString())),
        ],
      ),
    );
  }
}

class MapWidget extends StatefulWidget {
  @override
  MapWidgetState createState() => new MapWidgetState();
}

class MapWidgetState extends State<MapWidget> {
  Completer<GoogleMapController> _controller = Completer();
  LocationOptions _locationOp =
      new LocationOptions(accuracy: LocationAccuracy.best);
  GoogleMapController _controllerMap;
  Geolocator geo = new Geolocator();
  CameraPosition _initialPosition =
      CameraPosition(target: LatLng(22.7752698, -102.5642267), zoom: 18.0);
  double _lat, _lng, _latSensor, _lngSensor;
  bool _isDrivingSensor = false;
  LatLng _position = LatLng(0.0, 0.0);

  /*void _onMapCreated(GoogleMapController controller) {
    _controllerMap = controller;
  }*/

  void _updateMapFocus() {
    Future.delayed(Duration(seconds: 6), () {
      geo.getPositionStream(_locationOp).listen((event) {
        _controllerMap.animateCamera(CameraUpdate.newCameraPosition(
            CameraPosition(
                target: LatLng(event.latitude, event.longitude), zoom: 18.0)));
      });
    });
  }

  void _requestlatlngInitial() async {
    var temp =
        await geo.getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
    _lat = temp.latitude;
    _lng = temp.longitude;
  }

  void initState() {
    _requestlatlngInitial();
    _updateMapFocus();
  }

  @override
  Widget build(BuildContext context) {
    final sensor = Provider.of<GpsSensor>(context);

    return _lat == null || _lng == null
        ? Container()
        : GoogleMap(
            onMapCreated: (GoogleMapController controller) {
              _controllerMap = controller;
              //_controller.complete(controller);
            },
            initialCameraPosition:
                CameraPosition(target: LatLng(_lat, _lng), zoom: 18.0),
            myLocationButtonEnabled: true,
            myLocationEnabled: true,
          );
  }
}
