import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

class SpeedMeter extends StatefulWidget {
  SpeedMeter({Key key}) : super(key: key);

  @override
  _SpeedMeterState createState() => _SpeedMeterState();
}

class _SpeedMeterState extends State<SpeedMeter> {
  Geolocator geo;
  LocationOptions lo;
  Position p;

  @override
  void initState() {
    super.initState();
    geo = new Geolocator();
    lo = new LocationOptions(
        accuracy: LocationAccuracy.high);
    p = new Position();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Position>(
        stream: geo.getPositionStream(lo),
        initialData: new Position(speed: 0.0),
        builder: (BuildContext context, AsyncSnapshot<Position> snapshot) {
          var _speedToKmh;
          if (snapshot.hasData) {
            _speedToKmh = (snapshot.data.speed * 3.6).toInt();
            return Text(
              "$_speedToKmh",
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 15.0,
              ),
            );
          } else if (snapshot.hasError) {
            return Text(
              "No se ha podido acceder a la velocidad",
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 15.0,
              ),
            );
          }
          return Text(
            "$_speedToKmh",
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 15.0,
            ),
          );
        });
  }
}
