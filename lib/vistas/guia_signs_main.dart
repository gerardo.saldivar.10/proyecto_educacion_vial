import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tsp/backend/signs.dart';
import 'package:tsp/backend/sign_lists.dart';

List<String> categorias = [
  "Señales restrictivas",
  "Señales preventivas",
  "Señales de servicios",
  "Señales turisticas",
  "Otras señales"
];

List<Sign> restrictivas = SignLists.restrictivas;
List<Sign> preventivas = SignLists.preventivas;
List<Sign> servicios = SignLists.servicios;
List<Sign> turisticas = SignLists.turisticas;
List<Sign> otras = SignLists.otras;

List<Sign> actual;
Sign actualSign;

List<DropdownMenuItem<String>> _dropDownMenuItems;

String _currentSignList;

List<DropdownMenuItem<String>> getDropDownMenuItems() {
  List<DropdownMenuItem<String>> items = new List();
  for (String categoria in categorias) {
    items.add(new DropdownMenuItem(
        value: categoria,
        child: new Text(categoria)
    ));
  }
  return items;
}

class GuiaSignsMain extends StatefulWidget {
  @override
  _GuiaSignsMainState createState() => _GuiaSignsMainState();
}

class _GuiaSignsMainState extends State<GuiaSignsMain> {

  @override
  void initState() {
    _dropDownMenuItems = getDropDownMenuItems();
    _currentSignList = _dropDownMenuItems[0].value;
    actual = restrictivas;
    actualSign = actual[0];
    super.initState();
  }

  void changedDropDownItem(String selectedSignList) {
    setState(() {
      _currentSignList = selectedSignList;
      updateSignList();
      updateSign(0);
    });
  }

  void changedSign(index) {
    setState(() {
      updateSign(index);
    });
  }

  void updateSignList(){
    if(_currentSignList == "Señales restrictivas"){
      actual = restrictivas;
    }
    else if(_currentSignList == "Señales preventivas"){
      actual = preventivas;
    }
    else if(_currentSignList == "Señales de servicios"){
      actual = servicios;
    }
    else if(_currentSignList == "Señales turisticas"){
      actual = turisticas;
    }
    else{
      actual = otras;
    }
  }

  void updateSign(index) async {
    actualSign = actual[index];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        backgroundColor: Colors.blue[400],
        title: Text('Guía Rápida de Señales'),
        centerTitle: true,
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(
                color: Colors.grey[300],
                child: new DropdownButton(
                  value: _currentSignList,
                  items: _dropDownMenuItems,
                  onChanged: changedDropDownItem,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                  ),
                ),
              ),
            ),
            SizedBox(height: 10),
            Expanded(
              flex: 8,
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                      width: double.infinity,
                      color: Colors.grey[700],
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: <Widget>[
                            Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black,
                                    width: 4,
                                  ),
                                ),
                                child: Image.asset('assets/signs/${actualSign.image}')
                            ),
                            SizedBox(width: 10),
                            Container(
                              width: 200,
                              padding: EdgeInsets.all(10.0),
                              child: Text(
                                '${actualSign.id}\n\n${actualSign.title}',
                                style: TextStyle(
                                  fontSize: 22,
                                  color: Colors.white,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 10),
                    Container(
                      width: double.infinity,
                      color: Colors.grey[800],
                      child: Text(
                        '${actualSign.description}',
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                          height: 1.5,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: actual.length,
                itemBuilder: (context, index){
                  return Card(
                    child: RaisedButton(
                      onPressed: () {
                        changedSign(index);
                      },
                      child: Image.asset('assets/signs/${actual[index].image}'),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

