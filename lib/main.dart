import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tsp/backend/check_gasoline_meter.dart';
import 'package:tsp/backend/current_location_test.dart';
import 'package:tsp/backend/noise_checker.dart';
import 'package:tsp/vistas/pdf_reglamento_viewer.dart' as viewer;
import 'package:tsp/vistas/configuracion_main.dart';
import 'package:tsp/vistas/speedStreetVelocityView.dart';
import 'package:tsp/vistas/guia_signs_main.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tsp/backend/place_map.dart';
import 'package:tsp/backend/place_tracker_app.dart';
import 'package:tsp/backend/app_model.dart';
import 'package:tsp/vistas/vista_grafico.dart';
import 'dart:async';
import 'package:volume/volume.dart';
import 'package:tsp/backend/notifications.dart';
import 'package:tsp/backend/tipsMantVehicular.dart';
import 'package:tsp/vistas/licencia.dart';
import 'package:tsp/vistas/editar_licencia.dart';
import 'package:tsp/backend/place_map_o.dart';
import 'package:tsp/backend/place_tracker_app_o.dart';

Future<void> startServiceInPlatform() async {
  if (Platform.isAndroid) {
    var methodChannel = MethodChannel("test");
    String data = await methodChannel.invokeMethod("startService");
    print("En background");
    debugPrint(data);
  }
}

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MainApp());
}

class MainApp extends StatefulWidget{
 @override
  MainAppState createState() => MainAppState();
}

class MainAppState extends State<MainApp> {
  AppState appState = AppState();
  AppStateO appStateO = AppStateO();
  AudioManager audioManager;
  int maxVol, currentVol;
  ShowVolumeUI showVolumeUI = ShowVolumeUI.SHOW;
  Notifications notifications;
  Timer _t;

  @override
  void initState() {
    super.initState();
    audioManager = AudioManager.STREAM_MUSIC;
    //initAudioStreamType();
    //updateVolumes();
    notifications = new Notifications();
    checkSharedPreferences();
  }

  void checkSharedPreferences() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    int ultimoMantenimiento = pref.getInt("ultimoMantenimiento");
    int tiempoMantenimiento = pref.getInt("tiempoMantenimiento");
    if(ultimoMantenimiento == null) pref.setInt("ultimoMantenimiento", DateTime.now().millisecondsSinceEpoch);
    if(tiempoMantenimiento == null) pref.setInt("tiempoMantenimiento", Duration(days: 1).inMilliseconds);
  }

  Future<void> initAudioStreamType() async {
    await Volume.controlVolume(AudioManager.STREAM_MUSIC);
  }

  updateVolumes() async {
    // get Current Volume
    currentVol = await Volume.getVol;
    setState(() {});
    _t = Timer.periodic(Duration(seconds: 5), (timer) async {
      currentVol = await Volume.getVol;
      try{
        if(currentVol>10) {
          print("hola");
          //notifications.notificacionVolumenAlto();
        }
      } catch (exception) {
        _t.cancel();
      }
    });
  }

  setVol(int i) async {
    await Volume.setVol(i, showVolumeUI: showVolumeUI);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/home',
      routes: {
        '/home': (context) => MainPageHome(),
        '/guiasignsmain': (context) => GuiaSignsMain(),
        '/reglamentomain': (context) => viewer.MyApp(),
        '/tipsMantVehicular' : (context) => MantenimientoVehicular(),
        '/configuracionmain': (context) => ConfiguracionMain(notifications: notifications,),
        '/estadisticasManejo': (context) => WidgetGraficas(),
        '/perfil': (context) => Licencia(),
        '/perfil/editar': (context) => EditarLicencia(),
        '/talleres': (context) => PlaceMap(center: const LatLng(22.7752698, -102.5642267)),
        '/oficina': (context) => PlaceMapO(center: const LatLng(22.7678382,-102.5928404)),
      },

      builder: (context, child) {
        return AppModel<AppState>(initialState: AppState(), child: child,);
      },
    );
  }
}

class MainPageHome extends StatelessWidget {
  const MainPageHome({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => new GpsSensor()),
        ChangeNotifierProvider(create: (_) => new NoiseMeterChecker()),
        ChangeNotifierProvider(create: (_) => new CheckGasolineMeter()),
      ],
      child: SafeArea(
        child: Scaffold(
          floatingActionButton: FloatingActionButton(
            onPressed: startServiceInPlatform,
          ),
          drawer: MenuLateral(),
          
          appBar: AppBar(
            centerTitle: true,
            elevation: 2.5,
            title: Text("Educación vial APP"),
            
          ),
          body:SpeedStreetVelocityView(), 
          //MaxLimitSpeedStreet(),
        ),
      ),
    );
  }
}

/*class MainPage extends StatelessWidget {
  MainPage({Key key}) : super(key: key);

  @override
  void initState() {
    //startServiceInPlatform();
  }

  @override
  Widget build(BuildContext context) {
    LocationTest lt = new LocationTest();
    return MultiProvider(
      providers: [
        /*StreamProvider<Position>.value(
          initialData: Position(longitude: 0.0, latitude: 0.0),
          value: lt.positionStream,
          child: LabelTest(),
          ),*/
        ChangeNotifierProvider(
          create: (_) => lt,
        ),
      ],
      child: MaterialApp(
        home: Main(),
      ),
    );
  }
}*/

class Main extends StatelessWidget {
  const Main({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Texto de prueba'),
      ),
      body: Center(
        child: LabelTest(),
      ),
    );
  }
}

class LabelTest extends StatelessWidget {
  const LabelTest({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final temp = Provider.of(context);

    return Column(children: [
      Text(
        '${temp.speed}',
        style: TextStyle(
          fontSize: 25,
        ),
        textAlign: TextAlign.center,
      ),
      Text(
        '${temp.lat}, ${temp.lng}',
        style: TextStyle(
          fontSize: 25,
        ),
        textAlign: TextAlign.center,
      ),
      Text('${temp.permissions}'),
      FloatingActionButton(
        onPressed: () async {
          /*var temp = await Permission.location.status;
          print(temp);*/
          //temp.Dispose();
          //temp.CheckService();
          startServiceInPlatform();
        },
      ),
    ]);
  }
}


class MenuLateral extends StatelessWidget{
  @override
  Widget build (BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text('Nombre Ususario N'),
            accountEmail: Text('nombreU@correo.com'),
            currentAccountPicture: CircleAvatar(
              backgroundColor: Colors.amber,
              child: Text(
                'N',
                style: TextStyle(fontSize: 40.0),
              ),
            ),
          ),
          
          ListTile(
            title: Text('Perfil (Licencia)'),
            leading: Icon(Icons.person),
            onTap: (){
              Navigator.pushNamed(context, '/perfil');
            },
          ),
          ListTile(
            title: Text('Guia de señales'),
            leading: Icon(Icons.warning),
            onTap: (){
              Navigator.pushNamed(context, '/guiasignsmain');
            },
          ),

          ListTile(
            title: Text('Reglamento de la ley de tránsito del estado de Zacatecas'),
            leading: Icon(Icons.library_books),
            onTap: (){
              Navigator.pushNamed(context, '/reglamentomain');
            },
          ),
          ListTile(
            title: Text('Tips para Mantenimiento Vehicular'),
            leading: Icon(Icons.directions_car),
            onTap: (){
              Navigator.pushNamed(context, '/tipsMantVehicular');
            },
          ),
          ListTile(
            title: Text('Configuración'),
            leading: Icon(Icons.settings),
            onTap: () => Navigator.pushNamed(context, '/configuracionmain'),
          ),
          ListTile( 
            title: Text('Estadisticas de manejo'),
            leading: Icon(Icons.pie_chart),
            onTap: () => Navigator.pushNamed(context, '/estadisticasManejo'),
          ),
          ListTile(
            title: Text('Talleres mecanicos'),
            leading: Icon(Icons.build),
            onTap: () => Navigator.pushNamed(context, '/talleres'),
          ),
           ListTile(
            title: Text('Oficina de transito'),
            leading: Icon(Icons.location_on),
            onTap: () => Navigator.pushNamed(context, '/oficina'),
          ),
          Divider(),
        ],
      ),
    );
  }

}