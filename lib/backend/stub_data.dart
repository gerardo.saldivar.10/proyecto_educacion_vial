import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'place.dart';

class StubData {
  static const List<Place> places = [
    Place(
      id: '11',
      latLng: LatLng(22.7741614,-102.5791612),
      name: 'Servicio Man. Mecanico',
      description:
          'Reparacion de carros y mantenimiento.',
      category: PlaceCategory.wantToGo,
      starRating: 4,
    ),
    Place(
      id: '12',
      latLng: LatLng(22.7711107,-102.5816076),
      name: 'Taller mecanico electrico el ingeniero',
      description:
          'Reparacion de carros y mantenimiento.',
      category: PlaceCategory.wantToGo,
      starRating: 4,
    ),
    Place(
      id: '13',
      latLng: LatLng(22.7678224,-102.57268),
      name: 'Servicio automotriz rodriguez',
      description:
          'Reparacion de carros y mantenimiento.',
      category: PlaceCategory.wantToGo,
      starRating: 4,
    ),
    Place(
      id: '14',
      latLng: LatLng(22.7706814,-102.5763697),
      name: 'Taller mecanico male',
      description:
          'Reparacion de carros y mantenimiento.',
      category: PlaceCategory.wantToGo,
      starRating: 5,
    ),
    Place(
      id: '15',
      latLng: LatLng(22.770396,-102.5783934),
      name: 'Robles mecanicos',
      description:
          'Reparacion de carros y mantenimiento.',
      category: PlaceCategory.wantToGo,
      starRating: 5,
    ),
  ];

  static const List<String> reviewStrings = [
    'Buen lugar te atienden rápido y no es caro.',
    'Excelente atención, el mejor diagnóstico que me han dado. Precio justo',
    'Excelente  servicio'
  ];
}
