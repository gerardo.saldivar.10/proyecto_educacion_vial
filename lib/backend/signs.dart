class Sign {

  String id;
  String image;
  String title;
  String description;

  Sign({ this.id, this.image, this.title, this.description});
  
}