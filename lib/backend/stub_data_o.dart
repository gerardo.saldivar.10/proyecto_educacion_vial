import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'place_o.dart';

class StubData {
  static const List<Place> places = [
    Place(
      id: '11',
      latLng: LatLng(22.7678382,-102.5928404),
      name: 'Direccion de Policia de Seguridad Vial',
      description:
          'Oficina de gobierno local.',
      category: PlaceCategory.wantToGo,
      starRating: 4,
    ),
  ];

  static const List<String> reviewStrings = [
    'En esta oficina uno puede tramitar la licencia, hacer pagos y algunos otros trámites. La atención cuando fui fue amable y eficiente. Hay descuento en las multas si uno paga pronto.',
    'Staff was very friendly. Great atmosphere and good music. Would reccommend.',
    'Best. Place. In. Town. Period.'
  ];
}
