import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';

class DB {
  var database;
  var dbVersion;

  obtenerRutaDBEstadisticas() async {
    return openDatabase(
      // Set the path to the database. Note: Using the `join` function from the
      // `path` package is best practice to ensure the path is correctly
      // constructed for each platform.
      join(await getDatabasesPath(), 'ReeducaviTest2.db'),
      // When the database is first created, create a table to store dogs.
      onCreate: (db, version) => _createDB(db),
      //onUpgrade: (db, versionOld, newVersion) => _upgradeDB(db, versionOld, newVersion),
      // Set the version. This executes the onCreate function and provides a
      // path to perform database upgrades and downgrades.
      version: 1,
    );
  }

  static void _createDB(Database db) {
    db.execute(
        "CREATE TABLE fechas(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, dia TEXT, mes TEXT, anio TEXT, dateAsEpoch TEXT, calle TEXT)");
    db.execute(
        "CREATE TABLE licencias(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nombre TEXT, nacimiento TEXT, vencimiento TEXT)");
  }

  static Future<void> _upgradeDB(
      Database db, int oldVersion, int newVersion) async {
    Batch batch = db.batch();
    batch.execute('DROP TABLE IF EXISTS dogs');
    batch.execute(
        "CREATE TABLE dogs(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT, age INTEGER)");
    batch.execute(
        "CREATE TABLE fechas(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, dia TEXT, mes TEXT, anio TEXT, dateAsEpoch TEXT, calle TEXT)");
    batch.execute(
        "CREATE TABLE licencias(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nombre TEXT, nacimiento TEXT, vencimiento TEXT)");
    List<dynamic> result = await batch.commit();
  }

  Future<Database> get getDatabase => database;
  /*Future<Database> dataBase() async {
    return openDatabase(
      // Set the path to the database. Note: Using the `join` function from the
      // `path` package is best practice to ensure the path is correctly
      // constructed for each platform.
      join(await getDatabasesPath(), 'doggie_database.db'),
      // When the database is first created, create a table to store dogs.
      onCreate: (db, version) {
        return db.execute(
          "CREATE TABLE dogs(id INTEGER PRIMARY KEY, name TEXT, age INTEGER)",
        );
      },
      // Set the version. This executes the onCreate function and provides a
      // path to perform database upgrades and downgrades.
      version: 1,
    );
  }*/

  Future<bool> insertDate(Database db, dia, mes, anio, epoch) async {
    try{ 
      await db.insert(
        'fechas',
        {
          'dia': dia,
          'mes': mes,
          'anio': anio,
          'dateAsEpoch': epoch,
          'calle': 'No existe'
        },
        conflictAlgorithm: ConflictAlgorithm.ignore);
      return true;
    }catch(exception){
      print(exception);
    }
  }

  Future<void> insertLicencia(Database db, nombre, nacimiento, vencimiento) async {
    await db.rawQuery("REPLACE into licencias (id, nombre, nacimiento, vencimiento) VALUES (1,'$nombre','$nacimiento','$vencimiento')");
  }

  Future<List<Map<String, dynamic>>> consulta(Database db, String query) async {
    final Map<String, String> map = {};
    // Get a reference to the database.
    //final Database db = await database;

    final List<Map<String, dynamic>> fechasCount = await db.rawQuery(query);

    print(fechasCount);

    return fechasCount;
  }
}
