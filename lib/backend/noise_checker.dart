import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:noise_meter/noise_meter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:tsp/backend/notifications.dart';

class NoiseMeterChecker with ChangeNotifier {
  bool _notifEnabled;
  bool _isRecording = false;
  double _valueRecording = 0.0;
  Notifications notification;
  StreamSubscription<NoiseReading> _noiseSubscription;
  NoiseReading tempo;
  NoiseMeter _noiseMeter = new NoiseMeter();

  NoiseMeterChecker() {
    checkService();
    notification = new Notifications();
  }

  checkService() async {
    var status = await Permission.microphone.status;
    if (status.isGranted) {
      start();
    }
    if (status.isDenied) {
      var statusRequest = await Permission.microphone.request();
      if (statusRequest.isGranted) {
        start();
      } else {
        return;
      }
    }
    if (status.isUndetermined) {
      var statusRequest = await Permission.microphone.request();
      if (statusRequest.isGranted) {
        start();
      } else {
        return;
      }
    }
  }

  void start() async {
    var cooldown = 0;
    try {
      _noiseSubscription = _noiseMeter.noiseStream.listen((data) {
        _valueRecording = data.meanDecibel;
        if (_valueRecording >= 85 && _notifEnabled == true) {
          if (cooldown == 0) {
            notification.notificacionVolumenAlto();
            cooldown = 30;
          } 
        }

        if (cooldown > 0) {
          cooldown = cooldown - 1;
        } /*else {
          return;
        }*/
        notifyListeners();
      });
    } catch (exception) {
      print("Hay un error leyendo los datos");
      notifyListeners();
    }
  }

  double get valueRecording => _valueRecording;
  bool get isVoiceNotifEnabled => _notifEnabled;
  set voiceNotifCheckBox(bool value){
    _notifEnabled = value;
  }
}
