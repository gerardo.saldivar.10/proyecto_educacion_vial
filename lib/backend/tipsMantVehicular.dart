import 'dart:io';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_widget/carousel_widget.dart';

class MantenimientoVehicular extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title:
              Text("Mantenimiento Vehicular", style: TextStyle(fontSize: 18)),
        ),
        body: new Center(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Image.asset(
                "assets/tips/mantenimiento.png",
                height: 175,
                width: 175,
              ),
               Padding(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
              child: Text('La mayoría de los accidentes provocados por el vehículo podrían ' +
                    'haberse evitado si se hubiera llevado a cabo un correcto mantenimiento del coche.' +
                    ' Para que no te pase, te presentaré los 3 tipos principales de ' +
                    'mantenimiento: ',
              style: TextStyle(fontSize: 15), 
              textAlign: TextAlign.justify,)
            ),
              new Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  new RaisedButton(
                    child: new Text(
                      "Mantenimiento Preventivo",
                      style: TextStyle(fontSize: 12),
                    ),
                    textColor: Colors.black,
                    color: Colors.amber,
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MantenimientoPreventivo()));
                    },
                  ),
                  new RaisedButton(
                    child: Text(
                      "Mantenimiento Correctivo",
                      style: TextStyle(fontSize: 12),
                    ),
                    textColor: Colors.white,
                    color: Colors.red,
                    //padding: const EdgeInsets.all(8.0),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MantenimientoCorrectivo()));
                    },
                  ),
                  new RaisedButton(
                    child: Text(
                      "Mantenimiento Predictivo",
                      style: TextStyle(fontSize: 12),
                    ),
                    textColor: Colors.white,
                    color: Colors.lightBlue,
                    // padding: const EdgeInsets.all(8.0),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  VMantenimientoPredictivo()));
                    },
                  ),
                ],
              )
            ],
          ),
        ));
  }
}

class MantenimientoPreventivo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: Text("Mantenimiento Preventivo", style: TextStyle(fontSize: 18)),
      ),
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Image.asset(
              "assets/tips/prevencion.png",
              height: 250,
              width: 250,
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
              child: Text('El mantenimiento preventivo se refiere al seguimiento de' +
                   ' las instrucciones del fabricante en las que se indica los espacios '
                       'de tiempo o kilometraje en los que se deben sustituir ciertas partes '
                       'del vehículo o cada cuánto deben ser revisadas.',
              style: TextStyle(fontSize: 22), 
              textAlign: TextAlign.justify,)
            ),
          ],
        ),
      ),
    );
  }
}

class MantenimientoCorrectivo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: Text("Mantenimiento Correctivo", style: TextStyle(fontSize: 18)),
      ),
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Image.asset(
              "assets/tips/correctivo.jpg",
              height: 250,
              width: 250,
            ),
             Padding(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
              child: Text("Durante las tareas de mantenimiento correctivo tienen cabida"
              " las reparaciones o sustituciones de aquellos componentes del vehículo que"
              " han dejado de funcionar o ya no lo hacen adecuadamente",
              style: TextStyle(fontSize: 22), 
              textAlign: TextAlign.justify,)
            ),
          ],
        ),
      ),
    );
  }
}

class VMantenimientoPredictivo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    initializeData();

    return Scaffold(
      backgroundColor: Colors.white,
      body: Carousel(
        listViews: [
          Fragment(
            child: getScreen(0),
          ),
          Fragment(
            child: getScreen(1),
          ),
          Fragment(
            child: getScreen(2),
          ),
          Fragment(
            child: getScreen(3),
          ),
          Fragment(
            child: getScreen(4),
          ),
          Fragment(
            child: getScreen(5),
          ),
          Fragment(
            child: getScreen(6),
          ),
          Fragment(
            child: getScreen(7),
          ),
          Fragment(
            child: getScreen(8),
          ),
          Fragment(
            child: getScreen(9),
          ),
          Fragment(
            child: getScreen(10),
          ),
          Fragment(
            child: getScreen(11),
          ),
          Fragment(
            child: getScreen(12),
          ),
          Fragment(
            child: getScreen(13),
          )
        ],
      ),
    );
  }

  Widget getScreen(index) {
    return new ListView(
      children: <Widget>[
        new Container(
          height: 200.0,
          margin: const EdgeInsets.fromLTRB(20.0, 90.0, 20.0, 0.0),
          child: Image.asset(
            imageNames.elementAt(index),
          ),
        ),
        new Container(
          height: 45.0,
          margin: const EdgeInsets.fromLTRB(20.0, 8.0, 20.0, 0.0),
          child: Text(
            titles.elementAt(index),
            textAlign: TextAlign.center,
            overflow: TextOverflow.visible,
            style: TextStyle(fontSize: 20),
          ),
        ),
        new Container(
          height: 250.0,
          margin: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
          child: Text(
            description.elementAt(index),
            textAlign: TextAlign.justify,
            overflow: TextOverflow.visible,
            maxLines: 20,
            style: TextStyle(fontSize: 15),
          ),
        ),
      ],
    );
  }

  List<String> titles = List();
  List<String> description = List();
  List<String> imageNames = List();

  void initializeData() {
    //Fragment 0
    titles.add("Mantenimiento Predictivo");
    description.add(
        "El mantenimiento predictivo es cuando se realizan diagnósticos"
        " o mediciones que permiten predecir si es necesario realizar correcciones "
        "o ajustes antes de que ocurra una falla.");
    imageNames.add("assets/tips/predictivo.jpg");

    //Fragment 1
    titles.add("1.- Batería");
    description.add(
        "Primero, revisa periódicamente el exterior de tu Batería para "
        "asegurarte de que esté en buen estado, pues su vida útil depende del uso. "
        "Para mantenerla con carga, es recomendable usar moderadamente la iluminación "
        "interna, los focos, el sistema de sonido y la ventilación. Además, se aconseja "
        " llevarla a revisión cada tres meses.");
    imageNames.add("assets/tips/bateria.png");

    //Fragment 2
    titles.add("2.- Llantas");
    description.add(
        "Se recomienda revisarlas cada 10,000 km. Respecto al inflado "
        "de las llantas, revisa tu manual de usuario y mantenlas en la presión que "
        "se indica ahí, pues inflarlas demasiado puede provocarles cortes o rupturas"
        " al recibir un impacto, e inflarlas menos puede contribuir con el excesivo"
        " calentamiento y provocar daños estructurales internos que afecten también a"
        " otros sistemas. Se recomienda una revisión semanal.");
    imageNames.add("assets/tips/llantas.jpg");

    //Fragment 3
    titles.add("3.- Alineación y Balanceo");
    description.add("Suele hacerse junto con el cambio de neumáticos, pero es"
        " recomendable programarlo cada 10,000 km o si, al soltar el volante, "
        "éste gira hacia algún lado o hace ruidos de rodaje cuando elevas la velocidad.");
    imageNames.add("assets/tips/Alineacion_Balanceo.jpg");

    //Fragment 4
    titles.add("4.- Cadena o Correa de Distribución");
    description.add("La encargada de convertir la potencia del motor en el "
        "movimiento de tu auto es la correa o cadena de distribución. Al ser una"
        " parte tan importante, es recomendable revisarla con periodicidad y "
        "suele incluirse en el servicio general al motor, sin embargo, conviene"
        " que la cambies a los 90,000 y 160,000 km.");
    imageNames.add("assets/tips/cadena-correa-distribucion.jpg");

    //Fragment 5
    titles.add("5.- Embrague o Clutch");
    description.add("Es el sistema que da la señal de movimiento al auto de "
        "transmisión estándar, por lo tanto es importante estar siempre pendiente "
        "de la sensación que da al usarlo. Si el pedal se vuelve duro o comienza"
        " a dificultarse el paso de los cambios de velocidad, debes revisar el "
        "embrague. Su durabilidad se extiende hasta los 80,000 km, "
        "pero revísalo cada 30,000, pues tus hábitos al conducir serán "
        "determinantes para el desgaste, por ejemplo: si apoyas demasiado el pie"
        " en el pedal de manera constante, es posible que reduzcas la vida útil "
        "del sistema. Intenta poner Neutral todas las veces que te sea posible "
        "y arrancar suavemente.");
    imageNames.add("assets/tips/clutch.png");

    //Fragment 6
    titles.add("6.- Amortiguadores");
    description.add(
        "La frecuencia indicada para inspeccionar tus amortiguadores"
        " es de 20,000 km o antes si hay ruidos, golpes o inestabilidad. "
        "El cambio puede ir de los 40,000 km a los 80,000 km dependiendo de qué"
        " tan reciente sea el vehículo.");
    imageNames.add("assets/tips/amortiguador.jpg");

    //Fragment 7
    titles.add("7.- Pastillas de Freno");
    description.add("Aunque (como todo) depende del uso que les des, la "
        "inspección se recomienda a los 10,000 km y cambiar las pastillas a los "
        "25,000 km, en ella revisarán tanto el desgaste como la cantidad de "
        "líquido. Un indicador para cambiarlas puede ser la presencia de ruidos "
        "o silbidos y podría desencadenar desperfectos en otras partes del "
        "sistema. También considera que las pastillas delanteras se gastan más "
        "que las traseras.");
    imageNames.add("assets/tips/pastillas-freno.jpg");

    //Fragment 8
    titles.add("8.- Filtros de Aire, Aceite y Filtro de Gasolina");
    description.add("Los filtros se encargan de retener las impurezas para que"
        " no pasen al motor ni contaminen la cámara de combustión y los "
        "cilindros. Se recomienda cambiar los de aire cada año o a los  20,000 "
        "km, según el tipo de desgaste. Cambiar los filtros de aceite cada 10,000 "
        "km o al cambiar el aceite y el de combustible cada 30,000 km para usar "
        "sus capacidades al máximo.");
    imageNames.add("assets/tips/filtros a,a, g.jpg");

    //Fragment 9
    titles.add("9.- Aceite del Motor, Líquido de Transmisión y Anticongelante");
    description.add('La correcta lubricación del auto previene el desgaste '
        'prematuro, es por eso que hay que revisarlo constantemente y rellenarlo'
        ' sin pasarse de la línea de límite. El cambio de aceite en algunos '
        'autos recientes puede necesitarse a los 30,000 km. Sin embargo, en '
        'automóviles de más de 10 años puede llegar a requerirse desde los 7,000'
        ' km. Ten en cuenta que los líquidos como el de transmisión y el '
        'anticongelante también requieren monitoreo y debes cambiarlos '
        'regularmente.');
    imageNames.add("assets/tips/aceite-liquidos.jpg");

    //Fragment 10
    titles.add("10.- Combustible");
    description.add(
        'Mantener alto el nivel de combustible supone varias ventajas'
        ' para el cuidado general de tu auto, por ejemplo, evita que las impurezas '
        'asentadas en el fondo del tanque salgan y provoquen fallas en el motor,'
        ' además, el aire que entra al tanque favorece la evaporación, es decir,'
        ' al gasto acelerado de gasolina. Además, es aconsejable usar el Control'
        ' de Velocidad Crucero Adaptativo, que no sólo hará más cómoda tu '
        'experiencia al manejar, también ayudará a mejorar la velocidad y '
        'seguridad de tu viaje, reduciendo el consumo de gasolina.');
    imageNames.add("assets/tips/combustible.jpg");

    //Fragment 11
    titles.add("11.- Plumillas de limpia-parabrisas");
    description.add(
        'Este es un cambio de los más evidentes, ya que afecta directamente'
        'a la visibilidad de la conducción, además estas son de las piezas más afectadas '
        'por el ambiente. El calor excesivo y las lluvias suelen ser aceleradores del '
        'desgaste, por lo tanto, debes cambiarlos cada año entre el otoño y la primavera, '
        'además de revisarlos cada 6 meses.');
    imageNames.add("assets/tips/limpiaparabrisas.png");

    //Fragment 12
    titles.add("12.- Escape");
    description.add(
        'Para revisar el escape sólo debes revisar que el mofle no tenga'
        ' agujeros que causen una falla en su función de silenciador.'
        ' Si notas que sale humo negro por el escape hay que llevar a afinar el motor.'
        ' Es importante revisar regularmente este sistema, ya que su buen funcionamiento'
        ' es bueno para nuestra seguridad, nuestro bolsillo y para el medio ambiente.'
        ' Revisa el sistema de escape de tu vehículo cada 20.000 Km o una vez al año. Los '
        'elementos de sujeción y amarre también deben ser inspeccionados en ese mismo intervalo.');
    imageNames.add("assets/tips/escape.png");

    //Fragment 13
    titles.add("13.- Iluminación");
    description.add(
        'Inspecciona constantemente las luces de tu coche y si notas opacidad'
        ' o algún segmento fundido reemplázalo lo antes posible, pues es vital la visibilidad en el camino.'
        ' Sólo tómate 5 minutos para ver que todas las luces respondan a sus funciones, si notas que alguna está '
        'fundida cambiala por las que recomiende el fabricante. Las luces medias y altas deben alinearse cada seis meses');
    imageNames.add("assets/tips/luces.jpg");
  }
}
