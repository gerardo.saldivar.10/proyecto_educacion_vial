import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:geolocator/geolocator.dart';
import 'package:location/location.dart' as locationTest;
import 'package:tsp/backend/data_get_api.dart';
import 'package:tsp/backend/database.dart';
import 'package:tsp/backend/notifications.dart';

class GpsSensor with ChangeNotifier {
  bool _notifEnabled;
  Notifications notifications;
  locationTest.Location loc = new locationTest.Location();
  DataGetApi _getInfoApi;
  Timer _t;
  StreamSubscription _velocity;
  locationTest.PermissionStatus _appPermissions;
  bool _serviceEnabled, _isDriving, _isBadDirection;
  Geolocator location, pos;
  GeolocationStatus _permissions;
  Stream<Position> actualPosition;
  double _lat = 0.0, _lng = 0.0;
  int _currentSpeed = 0;
  String _speedStreet = "";

  GpsSensor() {
    _getInfoApi = new DataGetApi();
    location = new Geolocator();
    pos = new Geolocator();
    notifications = new Notifications();
    actualPosition = new Geolocator().getPositionStream(
      LocationOptions(accuracy: LocationAccuracy.bestForNavigation),
    );
    CheckService();
  }

  VelocidadActual(locationTest.PermissionStatus geo) {
    var counter = 0;
    if (geo == locationTest.PermissionStatus.granted &&
        _serviceEnabled == true) {
      try {
        _velocity = actualPosition.listen((event) {
          _currentSpeed = velToKms(event.speed);
          if (_currentSpeed >= 20) {
            _isDriving = true;
          } else {
            _isDriving = false;
          }

          /*if(counter == 10 || counter == 20 || counter == 30 || counter == 45){
            notifications.showNotificationWithSound();
          }
          if (_speedStreet != "0" && _speedStreet != null && _speedStreet != ""){
            if((int.parse(_speedStreet) - _currentSpeed) <= -10){
              notifications.showNotificationWithSound();
            }else{
              notifications.cancelNotification();
            }
          }*/
          notifyListeners();
        });
      } catch (exception) {
        _velocity.cancel();
      }
    } else {
      print("nothing");
    }
  }

  int velToKms(double speed) {
    speed = (speed * 3.6);
    return speed.toInt();
  }

  PosicionActual(locationTest.PermissionStatus geo) {
    if (geo == locationTest.PermissionStatus.granted &&
        _serviceEnabled == true) {
      _t = Timer.periodic(Duration(seconds: 3), (timer) async {
        try {
          if (_isDriving == true) {
            pos
                .getCurrentPosition(
                    desiredAccuracy: LocationAccuracy.bestForNavigation)
                .then((value) async {
              _speedStreet = await _getInfoApi.calculaVelocidadCalle(
                  value.latitude.toString(), value.longitude.toString());
              _isBadDirection =
                  await _getInfoApi.calculateIsBadDirection(null, null);
              _lat = value.latitude;
              _lng = value.longitude;

              if (_notifEnabled == true) {
                if ((int.parse(_speedStreet) - _currentSpeed) <= -10) {
                  notifications.notificacionExcesoVelocidad();
                  DB c = new DB();
                  var temp = await c.obtenerRutaDBEstadisticas();
                  var timezone = DateTime.now();
                  var timeZoneIso = timezone.toIso8601String();
                  c.insertDate(temp, timezone.day, timezone.month, timezone.year, timeZoneIso);
                } else {
                  return;
                }
              } else {
                return;
              }
              notifyListeners();
            });
          } else {
            _speedStreet = "0";
            notifyListeners();
          }
        } catch (exception) {
          _t.cancel();
        }
      });
    } else if (geo == locationTest.PermissionStatus.denied ||
        geo == locationTest.PermissionStatus.deniedForever ||
        _serviceEnabled == false) {}
  }

  CheckService() async {
    _serviceEnabled = await loc.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await loc.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }
    CheckingPermissions();
    notifyListeners();
  }

  Dispose() {
    _velocity.cancel();
    _t.cancel();
    notifyListeners();
  }

  CheckingPermissions() async {
    _appPermissions = await loc.hasPermission();
    if (_appPermissions == locationTest.PermissionStatus.denied) {
      _appPermissions = await loc.requestPermission();
      if (_appPermissions == locationTest.PermissionStatus.denied) {
        return;
      }
    }
    VelocidadActual(_appPermissions);
    PosicionActual(_appPermissions);
    notifyListeners();
  }

  set voiceNotifCheckBox(bool value) {
    _notifEnabled = value;
  }

  bool get isVoiceNotifEnabled => _notifEnabled;
  Stream<Position> get positionStream => actualPosition;
  GeolocationStatus get permissions => _permissions;
  int get speed => _currentSpeed;
  String get speedStreet => _speedStreet;
  bool get isDriving => _isDriving;
  bool get isBadDirection => _isBadDirection;
  double get lat => _lat;
  double get lng => _lng;
}
