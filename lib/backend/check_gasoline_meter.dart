import 'package:flutter/foundation.dart';
import 'package:tsp/backend/notifications.dart';

class CheckGasolineMeter with ChangeNotifier {
  bool _notifEnabled;
  Notifications notification;

  CheckGasolineMeter() {
    notification = new Notifications();
    alerta();
  }

  alerta() async{
    notification.notificacionGasolina();
  }
  /*alerta() {
    try {
      if (_notifEnabled == true) {
        notification.notificacionGasolina();
      }
      notifyListeners();
    } catch (exception) {
      print("Hay un error");
      notifyListeners();
    }
  }*/

  /*Dispose() {
    alerta().cancel();
    notifyListeners();
  }*/

  set voiceNotifCheckBox(bool value) {
    _notifEnabled = value;
  }

  bool get isVoiceNotifEnabled => _notifEnabled;
}