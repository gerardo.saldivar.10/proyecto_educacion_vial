import 'package:flutter_test/flutter_test.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:flutter/material.dart';


class MyNotification extends Notification {}

  void main() {
  testWidgets('Notification - Limite de velocidad', (WidgetTester tester) async {
    final List<dynamic> log = <dynamic>[];
    final GlobalKey key = GlobalKey();
    await tester.pumpWidget(NotificationListener<MyNotification>(
      onNotification: (MyNotification value) {
        log.add(value);
        return true;
      },
      child: NotificationListener<MyNotification>(
        onNotification: (MyNotification value) {
          log.add('Estas excediendo el limite de velocidad');
          log.add(value);
          return false;
        },
        child: Container(key: key),
      ),
    ));
    expect(log, isEmpty);
    final Notification notification = MyNotification();
    expect(() { notification.dispatch(key.currentContext); }, isNot(throwsException));
    expect(log, <dynamic>['Estas excediendo el limite de velocidad', notification , notification]);
  });
 }

// GoogleMapController mapController;
// class Ubicacion {
//   Location loc = new Location();
//   getPosition() async {
//     var pos = await loc.getLocation();
//     mapController.animateCamera(CameraUpdate.newCameraPosition(
//       CameraPosition(
//           target: LatLng(pos.latitude, pos.latitude),
//           zoom: 17.0,
//         )
//       )
//     );
//   }

//   void main() {
//   test('Prueba: obtener posicion actual',  () async {
//     double zoom = 17.0;
//     double longuitud = 22.7725949;
//     double latitud = -102.5739238;
//     CameraPosition(
//       target: LatLng(latitud, longuitud),
//       zoom: zoom);
//       }
//   );
//   expect(loc.getLocation(), CameraPosition(target: LatLng(22.7725949, -102.5739238), zoom: 17));
//   expect(CameraPosition, isNull);
//   }
// }