import 'package:flutter_test/flutter_test.dart';
import 'package:tsp/vistas/editar_licencia.dart';
import 'package:tsp/backend/notifications.dart';
import 'package:intl/intl.dart';

void main(){
  Notifications notifications;

  setUp((){
    notifications = new Notifications();
  });

  test("Debe regresar un boolean true si la alerta de licencia vencida se lanzó satisfactoriamente", () async{
    var _notification = await notifications.notificacionLicenciaVencida();
    expect(true, _notification);
  });


  test("Prueba que la fecha se transforme en el formato deseado", (){
    DateTime fecha = new DateTime(2020);
    final dateFormatDB = new DateFormat('dd-MM-yyyy');
    String fechaDB = dateFormatDB.format(fecha);
    expect(fechaDB, "01-01-2020");
  });
  
  test("Prueba que un texto en el formato deseado se pueda convertir a una fecha", (){
    String fechaDB = "11-06-2020";
    final dateFormatDB = new DateFormat('dd-MM-yyyy');
    DateTime fecha = dateFormatDB.parse(fechaDB);
    expect(fecha.toString(), "2020-06-11 00:00:00.000");
  });

}