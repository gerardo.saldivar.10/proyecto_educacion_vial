import 'package:flutter_test/flutter_test.dart';
import 'package:tsp/backend/signs.dart';
import 'package:tsp/backend/sign_lists.dart';
import 'package:tsp/vistas/guia_signs_main.dart';

void main(){
  setUp((){
    getDropDownMenuItems();
  });

  test("Prueba que el menú de señales no esté vacio", (){
    expect(getDropDownMenuItems().isNotEmpty, true);
  });
  
  test("Prueba que la primera categoría de señales sean las restrictivas", (){
    expect(getDropDownMenuItems().first.value, "Señales restrictivas");
  });

  test("Prueba que la última categoría de señales sean otras", (){
    expect(getDropDownMenuItems().last.value, "Otras señales");
  });

  test("Prueba que la señal que se muestre al iniciar sea la SR-6", (){
    actual = restrictivas;
    actualSign = actual[0];
    expect(actualSign.id,"SR-6");
  });

  test("Prueba que tome la señal correcta al cambiar de señal", (){
    actual = preventivas;
    actualSign = actual[2];
    expect(actualSign.id,"SP-8");
  });

  test("Prueba que la ruta a la imagen sea la correcta", (){
    actual = servicios;
    actualSign = actual[0];
    expect("assets/signs/SIS-1.png","assets/signs/${actualSign.image}");
  });

}