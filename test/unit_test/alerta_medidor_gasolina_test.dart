import 'package:flutter_test/flutter_test.dart';
import 'package:tsp/backend/notifications.dart';

void main(){
  Notifications notifications;

  setUp((){
    notifications = new Notifications();
  });

  test("Debe regresar un boolean true si la alerta para revisar el medidor de gasolina se lanzó satisfactoriamente", () async{
    var _notification = await notifications.notificacionGasolina();
    expect(true, _notification);
  });

}