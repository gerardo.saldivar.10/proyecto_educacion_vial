import 'package:flutter_test/flutter_test.dart';

void main(){
  test('Debe regresar que no necesita mantenimiento', () async {
    int now = DateTime.now().millisecondsSinceEpoch;
    // Simulamos que pasan 23 días
    int ultimoMantenimiento = DateTime.now().millisecondsSinceEpoch - 2000000000;
    int tiempoMantenimiento = 30; // 30 dias para dar mantenimiento
    int diasTranscurridos = 
      Duration(milliseconds: now).inDays - 
      Duration(milliseconds: ultimoMantenimiento).inDays;

    int tiempoRestante = tiempoMantenimiento - diasTranscurridos; 

    expect(tiempoRestante <= 0, false);
  });

  test('Debe regresar que necesita mantenimiento', () async {
    int now = DateTime.now().millisecondsSinceEpoch;
    // Simulamos que pasan 23 días
    int ultimoMantenimiento = DateTime.now().millisecondsSinceEpoch - 2000000000;
    int tiempoMantenimiento = 20; // 20 dias para dar mantenimiento
    int diasTranscurridos = 
      Duration(milliseconds: now).inDays - 
      Duration(milliseconds: ultimoMantenimiento).inDays;

    int tiempoRestante = tiempoMantenimiento - diasTranscurridos; 
  
    expect(tiempoRestante <= 0, true);
  });

}