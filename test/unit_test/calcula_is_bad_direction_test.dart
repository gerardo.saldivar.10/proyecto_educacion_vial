import 'package:flutter_test/flutter_test.dart';
import 'package:tsp/backend/data_get_api.dart' as dataGet;

void main(){
  test('Debe regresar un que va en direccion correcta', () async {
    bool func = await dataGet.DataGetApi().calculateIsBadDirection(270, ["E", "W"]);
    expect(func, false);
  });

  test('Debe regresar un que va en direccion incorrecta', () async {
    bool func = await dataGet.DataGetApi().calculateIsBadDirection(270, ["SE", "NW"]);
    expect(func, true);
  });
}