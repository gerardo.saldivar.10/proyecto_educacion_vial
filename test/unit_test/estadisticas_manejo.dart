import 'package:flutter_test/flutter_test.dart';
import 'package:tsp/backend/database.dart';

void main(){
  TestWidgetsFlutterBinding.ensureInitialized();

  var dbRoute;
  DB db;

  setUp(() async {
    db = new DB();
    dbRoute = await db.obtenerRutaDBEstadisticas();
  });

  test('Se deben poder ingresar datos con éxito a la bd', () async{
    var func = await db.insertDate(dbRoute, '29', '5', '2020', 'Epo');
    expect(func, true);
  });

  test('Se debe poder consultar con éxito una query', () async {
    var func = await db.consulta(dbRoute, "SELECT * FROM fechas");
    expect(func, isInstanceOf<List<Map<String, dynamic>>>());
  });
}